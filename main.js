'use strict'

const THEME = 'isThemeEnabled';
const baseURL = "api.shortify.info/";

document.addEventListener("DOMContentLoaded", handleOnload);
const themeToggle = document.getElementById('theme-toggle');
const htmlList = document.getElementById('search-list');

const store = {
  isError: false,
};

function searchHistory() {
  const list = Store().getHistory();
  if (!list) return;

  if (list.length) {
    htmlList.insertAdjacentHTML('beforebegin', '<strong>You already searched:</strong>');
  }
  const reversedList = list.reverse();
  for (let link of reversedList) {
    appendSearchItem(link)
  }
}

function appendSearchItem(link) {
    const listElement = `
    <li class="search-list-item">
      <div>
        ${link.longUrl} <br/>
        ${link.shortUrl}
      </div>
      <div>
        ${link.date}
      </div>
    </li>
  `;

  htmlList.insertAdjacentHTML('afterbegin', listElement)
}

// Theme
themeToggle.onclick = handleThemeChange;

function handleThemeChange(event) {
  const isThemeEnabled = event.target.checked;

  if (isThemeEnabled) {
    document.documentElement.setAttribute('data-theme', 'dark');
    return Store().setValue(THEME, true);
  }

  document.documentElement.setAttribute('data-theme', 'light');
  Store().setValue(THEME, false);
}

function handleOnload() {
  const isDarkTheme = Store().get(THEME);

  if (isDarkTheme) {
    document.documentElement.setAttribute('data-theme', 'dark');
    themeToggle.checked = true;
  } else {
    document.documentElement.setAttribute('data-theme', 'light');
    themeToggle.checked = false;
  }

  searchHistory();
}

// Fetch config
const api = () => {
  const post = (url, data) => fetch(
    `https://${baseURL}${url}`, 
    {
      method: 'POST',
      body: JSON.stringify(data),
      headers:{
        'Content-Type': 'application/json'
      },
    }
  )
  .then(res => {
    if (res.status >= 400 && res.status < 500) {
      return res.json().then(err => { throw (err) });
    }
  
    if (res.status >= 200 && res.status < 300) {
      return res.json();
    }
  })
  .catch(error => {
    throw (error)
  });

  const get = url => fetch(
    `${baseURL}${url}`, 
    {
      method: 'GET',
    }
  );

  return Object.freeze({
    GET: get,
    POST: post,
  });
}

const input = document.getElementById('parser-input');
input.onpaste = handlePostUrl();
input.oninput = debounce(handlePostUrl, 400);

async function handlePostUrl() {
  try {
    let urlValue;

    if (store.isError) {
      document.getElementById('input-errors').innerHTML = "";
    }

    if (!input.value || input.value.length < 9 || urlValue === input.value) {
      return;
    }

    const resp = await api().POST('url', { url: input.value.trim() });
    urlValue = input.value;
    const shortUrl = `${baseURL}${resp.shortUrl}`;

    const link = {
      shortUrl,
      longUrl: resp.longUrl,
      date: addFormatedDate(),
    }

    const historyData = Store().setHistory(link);
    if (historyData !== null) {
      appendSearchItem(link)
    }
   
    document.getElementById('shorten-url').innerHTML = shortUrl;
    const copyInput = document.getElementById('service-input');
    copyInput.value = resp.shortUrl;
  } catch (error) {
    store.isError = true;
    document.getElementById('shorten-url').innerHTML = '';
    if (error.message.url) {
      document.getElementById('input-errors').innerHTML = JSON.stringify(error.message.url);
    } else {
      console.log(error)
      document.getElementById('input-errors').innerHTML = 'Something went wrong';
    }
  }
}

// Helpers
function Store() {
  const history = 'history';

  function setHistory(value) {
    const historyStoreData = getHistory();
    let arrayVal = [];

    if (historyStoreData) {
      const isEqualItem = historyStoreData.find(data => value.longUrl === data.longUrl);

      if (isEqualItem) return null;

      // if (historyStoreData.length > 2) {
      //   const shiftedArray = historyStoreData.map((link, index) => {
      //     if (index === 0) {
      //       return value;
      //     }

      //     return link;
      //   });

      //   localStorage.setItem(history, JSON.stringify(shiftedArray));
      //   return shiftedArray;
      // }
  
      arrayVal = [ value, ...historyStoreData ];
      localStorage.setItem(history, JSON.stringify(arrayVal));
      return arrayVal;
    }

    arrayVal = [ value ];
    localStorage.setItem(history, JSON.stringify(arrayVal));
    return arrayVal;
  }

  function getHistory() {
    return JSON.parse(localStorage.getItem(history));
  }

  function get(key) {
    return JSON.parse(localStorage.getItem(key));
  }

  function setValue(key, value) {
    return JSON.stringify(localStorage.setItem(key, value));
  }

  return Object.freeze({
    setHistory,
    getHistory,
    get,
    setValue,
  });
}

function debounce(callback, time) {
  let interval;
  return () => {
    clearTimeout(interval);
    interval = setTimeout(() => {
      interval = null;
      callback(arguments);
    }, time);
  };
}

function copyText(url) {
  const copyInput = document.getElementById('service-input');
  copyInput.value = url;

  copyInput.focus();
  copyInput.select();
  
  const successful = document.execCommand('copy');
}

function addElement (type, content) { 
  const newElement = document.createElement(type); 
  const newContent = document.createTextNode(content); 
  newElement.appendChild(newContent);  
  return newElement;
}

function addFormatedDate() {
  const dateNow = new Date();
  const options = {
    weekday: 'short',
    month: 'long',
    day: 'numeric',
    hour12: false,
    hour: 'numeric', minute: 'numeric', second: 'numeric',
  };

  return new Intl.DateTimeFormat('en-GB', options).format(dateNow);
}
